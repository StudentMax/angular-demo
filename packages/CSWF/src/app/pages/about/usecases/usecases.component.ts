import {Component, OnInit} from '@angular/core';
import {UseCase} from '../usecase.model';

@Component({
    selector: 'app-about-usecases',
    templateUrl: './usecases.component.html',
    styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
    readonly PLAIN_USER = 'Reguliere gebruiker';
    readonly ADMIN_USER = 'Administrator';

    useCases: UseCase[] = [
        {
            id: 'UC-01',
            name: 'Inloggen',
            description: 'Hiermee logt een bestaande gebruiker in.',
            scenario: [
                'Gebruiker vult email en password in en klikt op Login knop.',
                'De applicatie valideert de ingevoerde gegevens.',
                'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
            ],
            actor: this.PLAIN_USER,
            precondition: 'Geen',
            postcondition: 'De actor is ingelogd',
        },
        {
            id: 'UC-02',
            name: 'registreren',
            description: 'Hiermee kan een nieuwe gebruiker een account aanmaken',
            scenario: [
                'Gebruiker vult zijn/haar informatie in en klikt op registreer knop.',
                'De applicatie valideert de ingevoerde gegevens.',
                'Indien gegevens correct zijn dan maakt de applicatie een nieuw account aan',
                'daarna redirect de applicatie naar het startscherm.'
            ],
            actor: this.PLAIN_USER,
            precondition: 'geen',
            postcondition: 'de nieuwe gebruiker heeft een account',
        },
        {
            id: 'UC-03',
            name: 'muziek toevoegen',
            description: 'Hiermee kan een gebruiker een nieuw nummer toevoegen. ',
            scenario: [
                'Gebruiker vult de  informatie van het nieuwe nummer in en klikt op registreer knop.',
                'De applicatie valideert de ingevoerde gegevens.',
                'Indien gegevens correct zijn dan maakt de applicatie een nieuw account aan',
                'daarna redirect de applicatie naar het startscherm.'
            ],
            actor: this.PLAIN_USER,
            precondition: 'geen',
            postcondition: 'de nieuwe gebruiker heeft een account',
        },
    ];

    constructor() {
    }

    ngOnInit(): void {
    }
}
